const Cardimage = ({src}) => {
  return(
    <div className="Card-image-design">
      <div className="Card-image">
        <img src={src} alt="user"/>
      </div>

      <div className="Card-description">
        <div className="flex Card-user">
          <p>Victor Crest</p>
          <p>26</p>
        </div>
        <p className="Card-user-location">London</p>
      </div>
          
    </div>
  )
}

export default Cardimage