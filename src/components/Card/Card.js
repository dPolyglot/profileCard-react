import './Card.scss';
import Cardimage from './Cardimage';
import image from '../../images/image-victor.jpg';


const Card = () => {
  const data = [
    {
      "id": 1,
      "numbers": 80,
      "type": "followers"
    },
    {
      "id": 2,
      "numbers": 803,
      "type": "likes"
    },
    {
      "id": 3,
      "numbers": 1.4,
      "type": "photos"
    }
  ]

  return(
    <>
      <div className="Card">
        <div className="Card-pattern"></div>
        <Cardimage src={image}/>
        <div className="flex Card-stats">
          {data.map((item, i) => 
            <div key={i} className="flex items">
              <p>{`${item.numbers}K`}</p>
              <p>{item.type}</p>
            </div>)
          }
        </div>
      </div>
    </>
  )
}

export default Card;