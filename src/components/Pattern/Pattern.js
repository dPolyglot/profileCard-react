const Pattern = ({src, alt, direction}) => {
  return <img className={`Pattern ${direction}`} src={src} alt={alt}/> 
}

export default Pattern;