import './App.scss';
import Pattern from './components/Pattern/Pattern';
import top from './images/bg-pattern-top.svg';
import bottom from './images/bg-pattern-bottom.svg';
import Card from './components/Card/Card'

function App() {
  return (
    <div className="App">
      <div className="App-pattern top">
        <Pattern src={top} alt="top"/>
      </div>

      <div className="App-pattern bottom">
        <Pattern src={bottom} alt="bottom"/>
      </div>
      <Card/>
    </div>
  );
}

export default App;
